package main

import (
	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/math32"
	"github.com/leonsal/godsp/audio"
	"github.com/leonsal/godsp/convert"
	"github.com/leonsal/godsp/core"
	"github.com/leonsal/godsp/fourier"
	"github.com/leonsal/godsp/operator"
	"github.com/leonsal/godsp/util"
	"github.com/leonsal/godsp/wavegen"
	"github.com/leonsal/godsp/winfunc"
)

// Register this test with main program
func init() {
	demoMap["2_signal_adder"] = &SignalAdderAudio{}
}

type SignalAdderAudio struct {
	ss       *core.Sources
	sig1     *wavegen.SignalVF32
	sig2     *wavegen.SignalVF32
	sig3     *wavegen.SignalVF32
	sink     *audio.AlSinkVF32
	ctime    *gui.Chart
	gtime    *gui.Graph
	chanTime *util.ChanVF32
	chanFreq *util.ChanVF32
	cfreq    *gui.Chart
	gfreq    *gui.Graph
}

// Start starts the test
func (t *SignalAdderAudio) Init(app *App) error {

	const fsize = 4 * 1024
	freq := uint(2000)
	ampl := float32(0.2)
	offset := float32(0)

	// Creates node graph
	t.ss = core.NewSources()

	// Creates signal 1
	t.sig1 = wavegen.NewSignalVF32(fsize)
	t.sig1.Input("frequency").(*core.InputUINT).Set(freq)
	t.sig1.Input("offset").(*core.InputF32).Set(offset)
	t.sig1.Input("samplerate").(*core.InputUINT).Set(app.SampleRate())
	t.sig1.Input("amplitude").(*core.InputF32).Set(ampl)
	t.sig1.Input("waveform").(*core.InputUINT).Set(wavegen.Sine)

	// Creates signal 2
	t.sig2 = wavegen.NewSignalVF32(fsize)
	t.sig2.Input("frequency").(*core.InputUINT).Set(freq)
	t.sig2.Input("offset").(*core.InputF32).Set(offset)
	t.sig2.Input("samplerate").(*core.InputUINT).Set(app.SampleRate())
	t.sig2.Input("amplitude").(*core.InputF32).Set(ampl)
	t.sig2.Input("waveform").(*core.InputUINT).Set(wavegen.Sine)

	// Creates signal 3
	t.sig3 = wavegen.NewSignalVF32(fsize)
	t.sig3.Input("frequency").(*core.InputUINT).Set(freq)
	t.sig3.Input("offset").(*core.InputF32).Set(offset)
	t.sig3.Input("samplerate").(*core.InputUINT).Set(app.SampleRate())
	t.sig3.Input("amplitude").(*core.InputF32).Set(ampl)
	t.sig3.Input("waveform").(*core.InputUINT).Set(wavegen.Sine)

	// Adds wave generator sources to the graph
	t.ss.Add(t.sig1)
	t.ss.Add(t.sig2)
	t.ss.Add(t.sig3)

	// Creates adder
	adder := operator.NewAdderVF32(3)
	t.sig1.Connect(adder.Input("0"))
	t.sig2.Connect(adder.Input("1"))
	t.sig3.Connect(adder.Input("2"))

	// Create audio sink and connects adder output to its input
	t.sink = audio.NewAlSinkVF32(2)
	t.sink.Input("samplerate").(*core.InputUINT).Set(app.SampleRate())
	t.sink.Input("gain").(*core.InputF32).Set(0)
	adder.Connect(t.sink.Input("data"))

	// Creates channel node for time graph and connects adder output to its input
	t.chanTime = util.NewChanVF32(10)
	adder.Connect(t.chanTime.Input("data"))

	// Creates converter of signal type VF32 to VC64
	// and connects adder output to its input
	convFreq := convert.NewVF32toVC64()
	adder.Connect(convFreq.Input("data"))

	// Creates window function for input to the FFT
	// and connects converter output to its input
	wfunc := winfunc.NewVC64(winfunc.Rectangular)
	convFreq.Connect(wfunc.Input("data"))

	// Creates FFT node and connects output of window function
	// to its input
	fft := fourier.NewFFTVC64(true)
	wfunc.Connect(fft.Input("data"))

	// Creates converter of C64 (output of FFT) to magnitude in F32
	// and connects fft output to its input
	convMag := convert.NewVC64MagF32()
	fft.Connect(convMag.Input("data"))

	// Creates channel node for frequency graph and connects signal to its input
	t.chanFreq = util.NewChanVF32(10)
	convMag.Connect(t.chanFreq.Input("data"))

	// PanelSignal common config
	sigConf := PanelSignalConfig{
		Freq:    2000,
		FreqMin: 0,
		FreqMax: 10000,
		Ampl:    0.2,
		AmplMin: 0,
		AmplMax: 1.0,
		Offset:  0,
		OffMin:  -1,
		OffMax:  1,
	}

	// Create UI for signal 1
	sigConf.Name = "sig1"
	sigConf.Sig = t.sig1
	uisig1 := NewPanelSignal(500, 32, &sigConf)
	app.demoPanel.Add(uisig1)

	// Create UI for signal 2
	sigConf.Name = "sig2"
	sigConf.Sig = t.sig2
	uisig2 := NewPanelSignal(500, 32, &sigConf)
	app.demoPanel.Add(uisig2)

	// Create UI for signal 3
	sigConf.Name = "sig3"
	sigConf.Sig = t.sig3
	uisig3 := NewPanelSignal(500, 32, &sigConf)
	app.demoPanel.Add(uisig3)

	// Create UI for window function selection for FFT
	uiwfunc := NewPanelWinfunc("winfunc", wfunc, 200, 32)
	app.demoPanel.Add(uiwfunc)

	// Creates time domain chart
	t.ctime = gui.NewChart(540, 200)
	t.ctime.SetTitle("Time Domain", 16)
	t.ctime.SetBorders(1, 1, 1, 1)
	t.ctime.SetBordersColor(math32.NewColor("Black"))
	t.ctime.SetColor(math32.NewColor("White"))
	t.ctime.SetPaddings(8, 8, 8, 8)
	t.ctime.SetScaleY(5, &math32.Color{0.8, 0.8, 0.8})
	t.ctime.SetFontSizeY(14)
	t.ctime.SetRangeX(0, 0, fsize)
	t.ctime.SetRangeY(-1, 1)
	t.ctime.SetFormatY("%2.1f")
	t.ctime.SetLayoutParams(&gui.VBoxLayoutParams{AlignH: gui.AlignWidth, Expand: 1})

	// Creates time domain graph
	t.gtime = t.ctime.AddLineGraph(&math32.Color{0, 0, 1}, nil)
	t.gtime.SetLineWidth(1.5)
	app.demoPanel.Add(t.ctime)

	// Creates frequency domain chart
	t.cfreq = gui.NewChart(540, 200)
	t.cfreq.SetTitle("Frequency Domain", 16)
	t.cfreq.SetBorders(1, 1, 1, 1)
	t.cfreq.SetBordersColor(math32.NewColor("Black"))
	t.cfreq.SetColor(math32.NewColor("White"))
	t.cfreq.SetPaddings(8, 8, 8, 8)
	t.cfreq.SetScaleY(5, &math32.Color{0.8, 0.8, 0.8})
	t.cfreq.SetFontSizeY(14)
	ndivs := 8
	t.cfreq.SetScaleX(ndivs, &math32.Color{0.8, 0.8, 0.8})
	// The value of each X division is:
	// (samplerate / fsize) * (fsize / ndivs) = samplerate / fsize
	// Divide by 2 because we are showing the first half of the data
	vdiv := float32(app.SampleRate()) / float32(ndivs) / 2
	t.cfreq.SetRangeX(0, vdiv/1000, float32(fsize/2)/float32(ndivs))
	t.cfreq.SetRangeY(0, fsize/2)
	t.cfreq.SetFormatX("%2.0fKhz")
	t.cfreq.SetFormatY("%2.0f")
	t.cfreq.SetLayoutParams(&gui.VBoxLayoutParams{AlignH: gui.AlignWidth, Expand: 1})

	// Creates frequency domain graph
	t.gfreq = t.cfreq.AddLineGraph(&math32.Color{0, 0, 1}, nil)
	t.gfreq.SetLineWidth(1.5)
	app.demoPanel.Add(t.cfreq)

	return nil
}

func (t *SignalAdderAudio) Status(app *App) uint32 {

	return t.ss.Status()
}

func (t *SignalAdderAudio) Start(app *App) error {

	return t.ss.Start()
}

func (t *SignalAdderAudio) Stop(app *App) error {

	return t.ss.Stop()
}

func (t *SignalAdderAudio) AudioGain(app *App, gain float32) error {

	t.sink.Input("gain").(*core.InputF32).Set(gain)
	return nil
}

// Close closes this test
func (t *SignalAdderAudio) Close(app *App) error {

	err := t.ss.Stop()
	t.sink.Dispose()
	return err
}

// Render is called before every OpenGL frame draw
func (t *SignalAdderAudio) Render(app *App) {

	// Reads adder output and sends to time graph
	data := t.chanTime.Peek()
	if data != nil {
		t.gtime.SetData(data)
	}

	// Reads FFT output and sends to frequency graph
	data = t.chanFreq.Peek()
	if data != nil {
		t.gfreq.SetData(data)
	}
}
