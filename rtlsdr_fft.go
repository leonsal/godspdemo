package main

import (
	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/math32"
	"github.com/leonsal/godsp/convert"
	"github.com/leonsal/godsp/core"
	"github.com/leonsal/godsp/fourier"
	"github.com/leonsal/godsp/radio"
	"github.com/leonsal/godsp/util"
	"github.com/leonsal/godsp/winfunc"
)

func init() {
	demoMap["3_rtlsdr_fft"] = &RtlsdrFFT{}
}

type RtlsdrFFT struct {
	ss       *core.Sources
	chanFreq *util.ChanVF32
	graph    *gui.Graph
}

// Init builds and intializes the demo
func (t *RtlsdrFFT) Init(app *App) error {

	// Creates sources manager
	t.ss = core.NewSources()

	// Creates RTLSDR radio source and adds it to the source manager
	rad, err := radio.NewRtlsdr(&radio.RtlsdrConfig{
		Index:    0,
		Bufcount: 4,
		Bufsize:  8 * (1 << 14), // N * 16Kbytes
	})
	if err != nil {
		return err
	}
	rad.Input("frequency").(*core.InputUINT).Write(89100000)
	t.ss.Add(rad)

	// Creates window function for input to the FFT
	// and connects the radio output to its input
	wfunc := winfunc.NewVC64(winfunc.Rectangular)
	rad.Connect(wfunc.Input)

	// Creates FFT node and connects output of window function to its input
	fft := fourier.NewFFTVC64(true)
	wfunc.Connect(fft.Input)

	// Creates converter of C64 (output of FFT) to magnitude in F32
	// and connects fft output to its input
	convMag := convert.NewVC64MagF32()
	fft.Connect(convMag.Input)

	// Creates channel node for frequency graph and connects converter output to its input
	t.chanFreq = util.NewChanVF32(1)
	convMag.Connect(t.chanFreq.Input)

	t.setUI(app)
	return nil
}

func (t *RtlsdrFFT) Status(app *App) uint32 {

	return t.ss.Status()
}

func (t *RtlsdrFFT) Start(app *App) error {

	return t.ss.Start()
}

func (t *RtlsdrFFT) Stop(app *App) error {

	return t.ss.Stop()
}

// Close closes this test
func (t *RtlsdrFFT) Close(app *App) error {

	return t.ss.Close()
}

func (t *RtlsdrFFT) AudioGain(app *App, gain float32) error {

	return nil
}

// Render is called before every OpenGL frame draw
// and transfers the FFT output to the chart
func (t *RtlsdrFFT) Render(app *App) {

	// Reads FFT output and sends to frequency graph
	data := t.chanFreq.Peek()
	if data != nil {
		app.log.Debug("data:%v", len(data))
		t.graph.SetData(data)
	}

}

func (t *RtlsdrFFT) setUI(app *App) {

	// Creates builder and parses declarative Gui
	builder := gui.NewBuilder()
	err := builder.ParseString(gui_data)
	if err != nil {
		app.log.Fatal("Error in declarative gui:%v", err)
	}

	// Builds toolbar
	tb, err := builder.Build("toolbar")
	if err != nil {
		app.log.Fatal("Error in building gui:%v", err)
	}
	app.demoPanel.Add(tb)

	// Builds chart
	ichart, err := builder.Build("chart")
	if err != nil {
		app.log.Fatal("Error in building gui:%v", err)
	}
	app.demoPanel.Add(ichart)
	chart := ichart.(*gui.Chart)

	// Creates line graph
	t.graph = chart.AddLineGraph(&math32.Color{0, 0, 1}, nil)
	t.graph.SetLineWidth(1.5)
	// The value of each X division is:
	// (samplerate / fsize) * (fsize / ndivs) = samplerate / fsize
	// Divide by 2 because we are showing the first half of the data
	ndivs := 5
	vdiv := float32(app.SampleRate()) / float32(ndivs) / 2
	fsize := 65536
	chart.SetRangeX(0, vdiv/1000, float32(fsize/2)/float32(ndivs))
}

const gui_data = `
toolbar:
  type: panel
  name: toolbar
  paddings: 2
  layoutparams:
    type: vbox
    alignh: width
  layout:
    type: hbox
    spacing: 2
    alignv: center
    autoheight: true
  items:
  - type: label
    text: "Frequency:"
  - type: hslider
    name: frequency
    text: " "
    width: 140

chart:
  type: chart
  name: fft
  title: FFT
  borders: 1 0 0 0
  layoutparams:
    type: vbox
    alignh: width
    expand: 1
  scalex:
    fontsize: 12
    margin: 20
    format: "%v"
    lines: 8
    color: blue
    rangemin: 0 
    rangemax: 60000
  scaley:
    fontsize: 12
    margin: 40
    format: "%v"
    lines: 5
    color: blue
    rangemin: 0 
    rangemax: 1000
`
