package main

import (
	"fmt"

	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/math32"
	"github.com/leonsal/godsp/core"
	"github.com/leonsal/godsp/wavegen"
)

type PanelSignal struct {
	gui.Panel
	c     PanelSignalConfig
	wlist *gui.DropDown
	fs    *gui.Slider
	as    *gui.Slider
	os    *gui.Slider
	//	freq    uint
	//	freqMin uint
	//	freqMax uint
	//	ampl    float32
	//	amplMin float32
	//	amplMax float32
	//	offset  float32
	//	offMin  float32
	//	offMax  float32
}

type PanelSignalConfig struct {
	Name    string
	Sig     *wavegen.SignalVF32
	Freq    uint
	FreqMin uint
	FreqMax uint
	Ampl    float32
	AmplMin float32
	AmplMax float32
	Offset  float32
	OffMin  float32
	OffMax  float32
}

// NewPanelSignal creates and returns an UI panel for the specified signal
func NewPanelSignal(width, height float32, c *PanelSignalConfig) *PanelSignal {

	s := new(PanelSignal)
	s.Panel.Initialize(width, height)
	s.c = *c
	s.Panel.SetBorders(1, 1, 1, 1)
	s.Panel.SetColor(math32.NewColor("White"))

	// Use horizontal box layout for this panel
	hbox := gui.NewHBoxLayout()
	hbox.SetSpacing(8)
	s.SetLayout(hbox)

	// Creates label with signal name
	lname := gui.NewLabel(s.c.Name)
	lname.SetPaddings(0, 0, 0, 4)
	lname.SetLayoutParams(&gui.HBoxLayoutParams{AlignV: gui.AlignCenter})
	s.Panel.Add(lname)

	// Creates dropdown list for waveform selection
	s.wlist = gui.NewDropDown(96, gui.NewImageLabel("sine"))
	s.wlist.SetLayoutParams(&gui.HBoxLayoutParams{AlignV: gui.AlignCenter})
	s.wlist.Add(gui.NewImageLabel("sine"))
	s.wlist.Add(gui.NewImageLabel("cosine"))
	s.wlist.Add(gui.NewImageLabel("square"))
	s.wlist.Add(gui.NewImageLabel("triangle"))
	s.wlist.Add(gui.NewImageLabel("sawtooth"))
	s.wlist.Add(gui.NewImageLabel("constant"))
	wmap := map[string]uint{
		"sine":     wavegen.Sine,
		"cosine":   wavegen.Cosine,
		"square":   wavegen.Square,
		"triangle": wavegen.Triangle,
		"sawtooth": wavegen.Sawtooth,
		"constant": wavegen.Constant,
	}
	s.wlist.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		sel := s.wlist.Selected()
		wname := sel.Text()
		wcode := wmap[wname]
		s.c.Sig.Input("waveform").(*core.InputUINT).Set(wcode)
	})
	s.Panel.Add(s.wlist)

	// Creates frequency slider
	s.fs = gui.NewHSlider(96, 20)
	s.fs.SetLayoutParams(&gui.HBoxLayoutParams{AlignV: gui.AlignCenter})
	s.Panel.Add(s.fs)
	s.fs.SetText(fmt.Sprintf("%d", s.c.Freq))
	v := float32(s.c.Freq-s.c.FreqMin) / float32(s.c.FreqMax-s.c.FreqMin)
	s.fs.SetValue(v)
	s.fs.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		s.c.Freq = uint(float32(s.c.FreqMin) + s.fs.Value()*float32(s.c.FreqMax-s.c.FreqMin))
		s.c.Sig.Input("frequency").(*core.InputUINT).Set(s.c.Freq)
		s.fs.SetText(fmt.Sprintf("%d", s.c.Freq))
	})
	s.Panel.Add(s.fs)

	// Creates amplitude slider
	s.as = gui.NewHSlider(96, 20)
	s.as.SetLayoutParams(&gui.HBoxLayoutParams{AlignV: gui.AlignCenter})
	s.Panel.Add(s.as)
	s.as.SetText(fmt.Sprintf("%3.1f", s.c.Ampl))
	av := (s.c.Ampl - s.c.AmplMin) / (s.c.AmplMax - s.c.AmplMin)
	s.as.SetValue(av)
	s.as.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		s.c.Ampl = s.c.AmplMin + s.as.Value()*(s.c.AmplMax-s.c.AmplMin)
		s.c.Sig.Input("amplitude").(*core.InputF32).Set(s.c.Ampl)
		s.as.SetText(fmt.Sprintf("%3.1f", s.c.Ampl))
	})
	s.Panel.Add(s.as)

	// Creates offset slider
	s.os = gui.NewHSlider(96, 20)
	s.os.SetLayoutParams(&gui.HBoxLayoutParams{AlignV: gui.AlignCenter})
	s.Panel.Add(s.os)
	s.os.SetText(fmt.Sprintf("%3.1f", s.c.Offset))
	ov := (s.c.Offset - s.c.OffMin) / (s.c.OffMax - s.c.OffMin)
	s.os.SetValue(ov)
	s.os.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		s.c.Offset = s.c.OffMin + s.os.Value()*(s.c.OffMax-s.c.OffMin)
		s.c.Sig.Input("offset").(*core.InputF32).Set(s.c.Offset)
		s.os.SetText(fmt.Sprintf("%3.1f", s.c.Offset))
	})
	s.Panel.Add(s.os)

	return s
}

func (s *PanelSignal) SetFrequencyRange(min, max uint) {

	s.c.FreqMin = min
	s.c.FreqMax = max
	v := float32(s.c.Freq-s.c.FreqMin) / float32(s.c.FreqMax)
	s.fs.SetValue(v)
}
