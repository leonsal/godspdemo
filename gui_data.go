package main

const guiData = `
#
# Top panel
#
type: panel
name: top
width: 600
height: 400
title: Window Title
layout:
  type: dock
items:
#
# Application header
#
- type: panel
  name: header
  borders: 0
  paddings: 6
  color: silver
  layoutparams:
    edge: top
  layout:
    type: hbox
    spacing: 2
    autoheight: true
  items:
  # app icon
  - type: imagelabel
    name: appicon
    icon: Settings
  # app name
  - type: label
    name: appname
    text: appname
  # app version label
  - type: imagelabel
    paddings: 0 0 0 4
    text: "v:"
  # app version label value
  - type: label
    name: appversion
    text: appversion
  # FPS label test
  - type: label
    name: fpslabel
    text: "FPS:"
    visible: false
    paddings: 0 0 0 10
  # FPS value
  - type: label
    name: fpsvalue
    text: fpsvalue
    visible: false

#
# Toolbar 
#
- type: panel
  name: toolbar
  borders: 0 0 1 0
  paddings: 4
  color: lightgray
  layoutparams:
    edge: top
  layout:
    type: hbox
    spacing: 8
    alignv: center
    autoheight: true
  items:
#
# Toolbar / buttons
#
  - type: Button
    name: start
    icon: PlayArrow
    enabled: false
    layoutparams:
      alignv: center
  - type: Checkbox
    name: audio
    text: Audio
    checked: false
    enabled: false
    layoutparams:
      alignv: center
#
# Center panel
#
- type: hsplitter
  name: center
  split: 0.3
  layoutparams:
    edge: center
  # Left panel
  panel0:
    name: panel0
    layout:
      type: vbox
    items:
    - type: tree
      name: tree
      layoutparams:
        alignh: width
        expand: 1
  panel1:
    name: panel1
    layout:
      type: vbox
#    items:
#    - type: panel
#      name: toolbar
#      width: 10
#      height: 10
#      borders: 1
#      color: red
#      layoutparams:
#        alignh: width
#        expand: 1  
`
