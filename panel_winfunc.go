package main

import (
	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/math32"
	"github.com/leonsal/godsp/winfunc"
)

type PanelWinfunc struct {
	gui.Panel
	label *gui.Label
	wlist *gui.DropDown
	wfunc *winfunc.VC64
}

// NewUiSignal creates and returns an UI panel for the specified signal
func NewPanelWinfunc(name string, wfunc *winfunc.VC64, width, height float32) *PanelWinfunc {

	s := new(PanelWinfunc)
	s.Panel.Initialize(width, height)
	s.wfunc = wfunc
	s.Panel.SetBorders(1, 1, 1, 1)
	s.Panel.SetColor(math32.NewColor("White"))

	// Use horizontal box layout for this panel
	hbox := gui.NewHBoxLayout()
	hbox.SetSpacing(8)
	s.SetLayout(hbox)

	// Creates label for name
	s.label = gui.NewLabel(name)
	s.label.SetLayoutParams(&gui.HBoxLayoutParams{AlignV: gui.AlignCenter})
	s.label.SetPaddings(0, 0, 0, 4)
	s.Panel.Add(s.label)

	// Creates dropdown list for waveform selection
	s.wlist = gui.NewDropDown(128, gui.NewImageLabel("rectangular"))
	s.wlist.SetLayoutParams(&gui.HBoxLayoutParams{AlignV: gui.AlignCenter})
	s.wlist.Add(gui.NewImageLabel("rectangular"))
	s.wlist.Add(gui.NewImageLabel("triangular"))
	s.wlist.Add(gui.NewImageLabel("hann"))
	s.wlist.Add(gui.NewImageLabel("hamming"))
	s.wlist.Add(gui.NewImageLabel("blackman"))
	s.wlist.Add(gui.NewImageLabel("blackman-harris"))
	s.wlist.Add(gui.NewImageLabel("flat top"))
	wmap := map[string]winfunc.Wintype{
		"rectangular":     winfunc.Rectangular,
		"triangular":      winfunc.Triangular,
		"hann":            winfunc.Hann,
		"hamming":         winfunc.Hamming,
		"blackman":        winfunc.Blackman,
		"blackman-harris": winfunc.BlackmanHarris,
		"flat top":        winfunc.FlatTop,
	}
	s.wlist.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		sel := s.wlist.Selected()
		wname := sel.Text()
		wcode := wmap[wname]
		s.wfunc.Type.Set(uint(wcode))
	})
	s.Panel.Add(s.wlist)

	return s
}
