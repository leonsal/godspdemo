package main

import (
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/gui/assets/icon"
	"github.com/g3n/engine/util/application"
	"github.com/g3n/engine/util/logger"
	"github.com/leonsal/godsp/core"
)

// Command line options
var (
	oSamplerate = flag.Uint("samplerate", 48000, "Audio sample rate")
	oShowFPS    = flag.Bool("showfps", false, "Show frames per second value in header")
)

// Current Version
const (
	progName = "GoDSP Demo"
	vMajor   = 0
	vMinor   = 1
	exeName  = "godspdemo"
)

type App struct {
	*application.Application                // Embedded application
	log                      *logger.Logger // Application logger
	header                   *gui.Panel
	appName                  *gui.Label
	appVersion               *gui.Label
	fpsLabel                 *gui.Label
	fpsValue                 *gui.Label
	startButton              *gui.Button     // Start/stop demo button
	audioCheck               *gui.CheckRadio // Audio checkbox
	tree                     *gui.Tree       // gui.Tree with tests names
	demoPanel                *gui.Panel      // gui.Panel for demo gui
	currentDemo              IDemo           // Current demo being executed
}

// IDemo is the interface for all demo objects
type IDemo interface {
	Init(*App) error               // Init builds the demo
	Start(*App) error              // Start runs the demo
	Stop(*App) error               // Stop stops running the demo
	Close(*App) error              // Close stops and disposes the demo
	Status(*App) uint32            // Returns demo running status
	AudioGain(*App, float32) error // Sets the audio gain
	Render(*App)                   // Render updates the demo chart
}

// demoMap maps the demo name to its object
var demoMap = map[string]IDemo{}

// main creates an instance of the application and runs it
func main() {

	app := Create()
	app.Run()
}

// Create creates and returns pointer to the application object
func Create() *App {

	// Sets the application usage
	flag.Usage = usage

	// Creates standard application object
	sa, err := application.Create(application.Options{
		Title:       "GoDSP Demo",
		Width:       800,
		Height:      600,
		Fullscreen:  false,
		LogPrefix:   "GODSPDEMO",
		LogLevel:    logger.DEBUG,
		TargetFPS:   60,
		EnableFlags: true,
	})
	if err != nil {
		panic(err)
	}
	app := new(App)
	app.Application = sa
	app.log = app.Log()
	app.log.Info("%s v%d.%d starting", progName, vMajor, vMinor)

	// Try to open audio device
	err = app.OpenDefaultAudioDevice()
	if err != nil {
		app.log.Error("%v", err)
	}

	app.buildGUI()

	// Select demo specified by the command line
	if len(flag.Args()) != 0 {
		name := flag.Args()[0]
		demo, ok := demoMap[name]
		if ok {
			app.initDemo(demo)
		}
	}
	return app
}

// SampleRate returns the current audio sample rate
func (app *App) SampleRate() uint {

	return *oSamplerate

}

// UpdateFPS updates the fps value in the application header
func (app *App) updateFPS() {

	// Get the FPS and potential FPS from the frameRater
	fps, pfps, ok := app.FrameRater().FPS(time.Duration(1000) * time.Millisecond)
	if !ok {
		return
	}

	// Shows the values in the window title or header label
	msg := fmt.Sprintf("%3.1f / %3.1f", fps, pfps)
	app.fpsValue.SetText(msg)
}

// buildGUI builds the application GUI
func (app *App) buildGUI() {

	// Creates builder and parses declarative Gui
	builder := gui.NewBuilder()
	err := builder.ParseString(guiData)
	if err != nil {
		app.log.Fatal("Error in declarative gui:%v", err)
	}

	// Builds top panel
	p, err := builder.Build("")
	if err != nil {
		app.log.Fatal("Error in building gui:%v", err)
	}
	top := p.(*gui.Panel)

	// Adds top panel to root panel
	app.Gui().SetLayout(gui.NewFillLayout(true, true))
	app.Gui().Add(top)

	// Get references to Gui objects
	app.header = top.FindPath("top/header").(*gui.Panel)
	app.appName = top.FindPath("top/header/appname").(*gui.Label)
	app.appVersion = top.FindPath("top/header/appversion").(*gui.Label)
	app.fpsLabel = top.FindPath("top/header/fpslabel").(*gui.Label)
	app.fpsValue = top.FindPath("top/header/fpsvalue").(*gui.Label)
	app.startButton = top.FindPath("top/toolbar/start").(*gui.Button)
	app.audioCheck = top.FindPath("top/toolbar/audio").(*gui.CheckRadio)
	app.tree = top.FindPath("top/center/panel0/tree").(*gui.Tree)
	app.demoPanel = top.FindPath("top/center/panel1").(*gui.Panel)

	// Sets applicaiton name and version
	app.appName.SetText(progName)
	app.appVersion.SetText(fmt.Sprintf("%d.%v", vMajor, vMinor))

	// If user requested to show Frames Per Second
	if *oShowFPS {
		app.fpsLabel.SetVisible(true)
		app.fpsValue.SetVisible(true)
		app.Subscribe(application.OnAfterRender, func(evname string, ev interface{}) {
			app.updateFPS()
		})
	}
	// Force header layout recalc (this should have been done by G3N..."
	app.header.Layout().Recalc(app.header)

	// Set event handler for Start/stop button
	app.startButton.Subscribe(gui.OnClick, func(evname string, ev interface{}) {
		if app.currentDemo == nil {
			return
		}
		var err error
		if app.currentDemo.Status(app) == core.Stopped {
			err = app.currentDemo.Start(app)
			app.startButton.SetIcon(icon.Pause)
		} else {
			err = app.currentDemo.Stop(app)
			app.startButton.SetIcon(icon.PlayArrow)
		}
		if err != nil {
			app.log.Error("%s", err)
		}
	})

	// Set event handler for audio checkbox
	app.audioCheck.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		if app.currentDemo == nil {
			return
		}
		if app.audioCheck.Value() {
			app.currentDemo.AudioGain(app, 1)
		} else {
			app.currentDemo.AudioGain(app, 0)
		}

	})

	// Sort demo names
	names := []string{}
	nodes := make(map[string]*gui.TreeNode)
	for name := range demoMap {
		names = append(names, name)
	}
	sort.Strings(names)

	// Add demos to the tree
	for _, name := range names {
		parts := strings.Split(name, ".")
		if len(parts) > 1 {
			category := parts[0]
			node := nodes[category]
			if node == nil {
				node = app.tree.AddNode(category)
				nodes[category] = node
			}
			labelText := strings.Join(parts[1:], ".")
			item := gui.NewLabel(labelText)
			item.SetUserData(demoMap[name])
			node.Add(item)
		} else {
			item := gui.NewLabel(name)
			item.SetUserData(demoMap[name])
			app.tree.Add(item)
		}
	}

	// Subscribe to tree selection
	app.tree.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		sel := app.tree.Selected()
		if sel == nil {
			return
		}
		label, ok := sel.(*gui.Label)
		if !ok {
			return
		}
		// If the selected demo is the current, ignore
		demo := label.GetNode().UserData().(IDemo)
		app.initDemo(demo)
	})

	// Subscribe to the application render loop event
	app.Subscribe(application.OnBeforeRender, func(evname string, ev interface{}) {
		if app.currentDemo == nil {
			return
		}
		app.currentDemo.Render(app)
	})
}

// initDemo selects and initializes the specified demo
func (app *App) initDemo(demo IDemo) {

	if app.currentDemo == demo {
		return
	}
	// Closes the current demo
	if app.currentDemo != nil {
		app.currentDemo.Close(app)
	}
	// Dispose of all previous test gui children
	app.demoPanel.DisposeChildren(true)

	// Initializes the selected demo
	app.currentDemo = demo
	app.currentDemo.Init(app)

	app.startButton.SetEnabled(true)
	app.startButton.SetIcon(icon.PlayArrow)
	app.audioCheck.SetEnabled(true)
	app.audioCheck.SetValue(false)
}

// usage shows application usage
func usage() {

	fmt.Fprintf(os.Stderr, "%s v%d.%d\n", progName, vMajor, vMinor)
	fmt.Fprintf(os.Stderr, "usage: %s [options]\n", exeName)
	flag.PrintDefaults()
	os.Exit(1)
}
