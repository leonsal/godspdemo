package main

import (
	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/math32"
	"github.com/leonsal/godsp/audio"
	"github.com/leonsal/godsp/core"
	"github.com/leonsal/godsp/util"
	"github.com/leonsal/godsp/wavegen"
)

func init() {
	demoMap["1_signal"] = &SignalAudio{}
}

type SignalAudio struct {
	ss    *core.Sources
	sig   *wavegen.SignalVF32
	sink  *audio.AlSinkVF32
	graph *gui.Graph
	ginp  *core.InputVF32
	nchan *util.ChanVF32
}

// Init builds and intializes the demo
func (t *SignalAudio) Init(app *App) error {

	const fsize = 4 * 1024
	freq := uint(2000)
	ampl := float32(0.5)
	offset := float32(0)

	// Creates source manager
	t.ss = core.NewSources()

	// Creates wave generator
	t.sig = wavegen.NewSignalVF32(fsize)
	t.sig.Input("frequency").(*core.InputUINT).Set(freq)
	t.sig.Input("offset").(*core.InputF32).Set(offset)
	t.sig.Input("samplerate").(*core.InputUINT).Set(app.SampleRate())
	t.sig.Input("amplitude").(*core.InputF32).Set(ampl)
	t.sig.Input("waveform").(*core.InputUINT).Set(wavegen.Sine)

	// Adds wave generator source to the graph
	t.ss.Add(t.sig)

	// Create audio sink and connects signal to its input
	t.sink = audio.NewAlSinkVF32(2)
	t.sink.Input("samplerate").(*core.InputUINT).Set(app.SampleRate())
	t.sink.Input("gain").(*core.InputF32).Set(0)
	t.sig.Connect(t.sink.Input("data"))

	// Creates channel node and connects signal to its input
	t.nchan = util.NewChanVF32(10)
	t.sig.Connect(t.nchan.Input("data"))

	//// Adds toolbar
	//tb := NewToolbar(240, 32, t.g, t.sink)
	//app.demoPanel.Add(tb)

	// Create UI for signal 1
	sigConf := PanelSignalConfig{
		Name:    "sig",
		Sig:     t.sig,
		Freq:    2000,
		FreqMin: 0,
		FreqMax: 10000,
		Ampl:    1.0,
		AmplMin: 0,
		AmplMax: 2.0,
		Offset:  0,
		OffMin:  -1,
		OffMax:  1,
	}
	uisig := NewPanelSignal(500, 32, &sigConf)
	app.demoPanel.Add(uisig)

	// Creates chart
	chart := gui.NewChart(540, 300)
	chart.SetBorders(1, 1, 1, 1)
	chart.SetBordersColor(math32.NewColor("Black"))
	chart.SetColor(math32.NewColor("White"))
	chart.SetPaddings(8, 8, 8, 8)
	chart.SetScaleY(5, &math32.Color{0.8, 0.8, 0.8})
	chart.SetFontSizeY(14)
	chart.SetRangeX(0, 0, fsize)
	chart.SetRangeY(-1, 1)
	chart.SetFormatY("%2.1f")
	chart.SetLayoutParams(&gui.VBoxLayoutParams{AlignH: gui.AlignWidth, Expand: 1})

	// Creates chart graph
	t.graph = chart.AddLineGraph(&math32.Color{0, 0, 1}, nil)
	t.graph.SetLineWidth(1.5)
	app.demoPanel.Add(chart)
	return nil
}

func (t *SignalAudio) Status(app *App) uint32 {

	return t.ss.Status()
}

func (t *SignalAudio) Start(app *App) error {

	return t.ss.Start()
}

func (t *SignalAudio) Stop(app *App) error {

	return t.ss.Stop()
}

// Close closes this test
func (t *SignalAudio) Close(app *App) error {

	err := t.ss.Close()
	t.sink.Dispose()
	return err
}

func (t *SignalAudio) AudioGain(app *App, gain float32) error {

	t.sink.Input("gain").(*core.InputF32).Set(gain)
	return nil
}

// Render is called before every OpenGL frame draw
func (t *SignalAudio) Render(app *App) {

	data := t.nchan.Peek()
	if data != nil {
		t.graph.SetData(data)
	}
}
